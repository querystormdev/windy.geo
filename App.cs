using Microsoft.Office.Interop.Excel;
using System;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using Unity;
using Unity.Lifetime;
using static QueryStorm.Tools.DebugHelpers;
using Jot;
using System.Windows;
using System.Windows.Controls;

namespace Windy.GeoCoding
{
    public class App : ApplicationModule, IConfigurableApp
    {
        public App(IAppHost appHost)
            : base(appHost)
        {
            // Register services here to make them available to components via DI, for example:
            // Container.RegisterInstance(new MyServiceAbc())
        }
    }
}