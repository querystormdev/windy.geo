using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Jot.Configuration.Attributes;
using QueryStorm.Apps;
using QueryStorm.Tools;
using Thingie.WPF.Controls.PropertiesEditor.DefaultFactory.Attributes;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.GeoCoding
{
	public class Config : ISettings
    {
        private string apiKey;

        [Trackable, Editable, Description("Enter your Google Cloud API key. ")]
        public string GoogleApiKey
        {
            get => apiKey;
            set { apiKey = value; }
        }
    }
}
