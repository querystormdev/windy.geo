using Geo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using QueryStorm.Apps;
using static QueryStorm.Tools.DebugHelpers;
using System.Globalization;

namespace Windy.GeoCoding
{
    public class GoogleGeocodeFunction
    {
        Dictionary<string, JArray> responses = new Dictionary<string, JArray>();

        private readonly Config config;

        public GoogleGeocodeFunction(Config config)
        {
            this.config = config;
        }

        [ExcelFunction(Name = "Windy.Geo.GoogleGeocode")]
        public async Task<string> GeoCode(string address)
        {
            try
            {
                var result = await GetGeocodeResults(address);
                if(result.Count == 0)
                	return "[Not found]";
                var location = result[0]["geometry"]["location"];       
                return $"{ToStringNum((double)location["lat"])}, {ToStringNum((double)location["lng"])}";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        
        NumberFormatInfo nfi = new NumberFormatInfo() { NumberDecimalSeparator = "."};
        private string ToStringNum(double d)
        {
        	return d.ToString(nfi);
        }

        private async Task<JArray> GetGeocodeResults(string address)
        {
            if (responses.TryGetValue(address, out var result))
                return result;

            string apiKey = config.GoogleApiKey;
            string encodedAddress = HttpUtility.UrlEncode(address);
            var url = $"https://maps.googleapis.com/maps/api/geocode/json?address={encodedAddress}&key={apiKey}";
            var client = new HttpClient();
            var res = await client.GetAsync(url);
            var payload = await res.Content.ReadAsStringAsync();

            if (res.IsSuccessStatusCode)
            {
                var jObj = JObject.Parse(payload);
                var err = jObj["error_message"]?.ToString();
                if (err != null)
                    throw new Exception(err);
                else
                    return responses[address] = jObj["results"] as JArray;
            }
            else
                throw new Exception($"Request failed with status {res.StatusCode}, message: {payload}");
        }
    }
}
