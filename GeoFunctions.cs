using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.Linq;
using System.Text;
using Geo;
using Geo.Measure;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.GeoCoding
{
	public class Class1
	{
		[ExcelFunction(Name = "Windy.Geo.ParseLatitude")]
		public static double ParseLatitude(string coordinateString)
		{
			var coord = Coordinate.Parse(coordinateString);
			return coord.Latitude;
		}
		
		[ExcelFunction(Name = "Windy.Geo.ParseLongitude")]
		public static double ParseLongitude(string coordinateString)
		{
			var coord = Coordinate.Parse(coordinateString);
			return coord.Longitude;
		}
		
		[ExcelFunction(Name = "Windy.Geo.Distance")]
		public static double Distance(string location1, string location2, string unit = "m")
		{			
			return new Geo.Geometries.LineString(Coordinate.Parse(location1), Coordinate.Parse(location2))
				.GetLength()
				.ConvertTo((DistanceUnit)Enum.Parse(typeof(DistanceUnit), unit, true))
				.Value;
		}
	}
}
