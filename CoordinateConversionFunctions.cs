using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Geo;
using ProjNet;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Geocoding
{
	public enum CoordinateSystems
	{
		WGS84,
		GK5,
		GK6,
		HTRS96,
		MGI1901_6
	}

    public class CoordinateConversionFunctions
    {
        const string HTRS = "PROJCS[\"HTRS96 / Croatia TM\",GEOGCS[\"HTRS96\",DATUM[\"Croatian_Terrestrial_Reference_System\",SPHEROID[\"GRS 1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY[\"EPSG\",\"6761\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4761\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",16.5],PARAMETER[\"scale_factor\",0.9999],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH],AUTHORITY[\"EPSG\",\"3765\"]]";
        const string GK5 = "PROJCS[\"DHDN / 3-degree Gauss-Kruger zone 5\",GEOGCS[\"DHDN\",DATUM[\"Deutsches_Hauptdreiecksnetz\",SPHEROID[\"Bessel 1841\",6377397.155,299.1528128,AUTHORITY[\"EPSG\",\"7004\"]],TOWGS84[598.1,73.7,418.2,0.202,0.045,-2.455,6.7],AUTHORITY[\"EPSG\",\"6314\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4314\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",15],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",5500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AUTHORITY[\"EPSG\",\"31469\"]]";
        const string GK6 = "PROJCS[\"Pulkovo 1942 / Gauss-Kruger zone 6\",GEOGCS[\"Pulkovo 1942\",DATUM[\"Pulkovo_1942\",SPHEROID[\"Krassowsky 1940\",6378245,298.3,AUTHORITY[\"EPSG\",\"7024\"]],AUTHORITY[\"EPSG\",\"6284\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4284\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",33],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",6500000],PARAMETER[\"false_northing\",0],AUTHORITY[\"EPSG\",\"28406\"],AXIS[\"Y\",EAST],AXIS[\"X\",NORTH]]";
        const string MGI1901_6 = "PROJCS[\"MGI 1901 / Balkans zone 6\",GEOGCS[\"MGI 1901\",DATUM[\"MGI_1901\",SPHEROID[\"Bessel 1841\",6377397.155,299.1528128,AUTHORITY[\"EPSG\",\"7004\"]],TOWGS84[682,-203,480,0,0,0,0],AUTHORITY[\"EPSG\",\"1031\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"3906\"]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",18],PARAMETER[\"scale_factor\",0.9999],PARAMETER[\"false_easting\",6500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AUTHORITY[\"EPSG\",\"3908\"]]";

		CoordinateSystemFactory f = new CoordinateSystemFactory();
        CoordinateSystemServices s = new CoordinateSystemServices();
		
		Dictionary<string, MathTransform> transforms = new Dictionary<string, MathTransform>();
		private MathTransform GetTransform(CoordinateSystem cs1, CoordinateSystem cs2)
		{
			var key = $"{cs1.Name} * {cs2.Name}";
			if(transforms.TryGetValue(key, out var transform))
				return transform;
			else
				return transforms[key] = s.CreateTransformation(cs1, cs2).MathTransform;
		}
		
        Dictionary<CoordinateSystems, CoordinateSystem> coordinateSystems = new Dictionary<CoordinateSystems, CoordinateSystem>();
		private CoordinateSystem GetCoordinateSystem(CoordinateSystems name)
		{
			if(coordinateSystems.TryGetValue(name, out var val))
				return val;
			else
				return coordinateSystems[name] = CreateCoordinateSystem(name);
		}
		
        private CoordinateSystem CreateCoordinateSystem(CoordinateSystems name)
        {
            switch (name)
            {
            	case CoordinateSystems.WGS84:
            		return GeographicCoordinateSystem.WGS84;
        		case CoordinateSystems.GK5:
        			return f.CreateFromWkt(GK5);
    			case CoordinateSystems.GK6:
        			return f.CreateFromWkt(GK6);
    			case CoordinateSystems.HTRS96:
        			return f.CreateFromWkt(HTRS);
				case CoordinateSystems.MGI1901_6:
					return f.CreateFromWkt(MGI1901_6);
                default:
                	throw new NotImplementedException();
            }
        }

		[ExcelFunction]
        public object[] TransformCoordinates(double coord1, double coord2, CoordinateSystems startSystem, CoordinateSystems destSystem)
        {
        	var cs1 = GetCoordinateSystem(startSystem);
        	var cs2 = GetCoordinateSystem(destSystem);
        	var t = GetTransform(cs1, cs2);
            (var x, var y) = t.Transform(coord1, coord2);
            return new object[] {x, y};
        }

        [ExcelFunction]
        public object [] WgsToHtrs(double lat, double lon)
        {
            return TransformCoordinates(lat, lon, CoordinateSystems.WGS84, CoordinateSystems.HTRS96);
        }

        [ExcelFunction]
        public object[] GK5ToHtrs(double x, double y)
        {
            return TransformCoordinates(x, y, CoordinateSystems.GK5, CoordinateSystems.HTRS96);
        }

        [ExcelFunction]
        public object[] GK6ToHtrs(double x, double y)
        {
            return TransformCoordinates(x, y, CoordinateSystems.GK6, CoordinateSystems.HTRS96);
        }

        [ExcelFunction]
        public object[] GK6ToGK5(double x, double y)
        {
            return TransformCoordinates(x, y, CoordinateSystems.GK6, CoordinateSystems.GK5);
        }
    }
}
